function updateTime() {
  const timeZones = {
    'Stockholm': 'Europe/Stockholm',
    'New York': 'America/New_York',
    'Beijing': 'Asia/Shanghai',
    'Cape Town': 'Africa/Johannesburg'
  };

  for (const [city, timeZone] of Object.entries(timeZones)) {
    const timeElement = document.getElementById(`time${city.replace(/\s/g, '')}`);
    if (timeElement) {
      const time = new Date().toLocaleTimeString('en-US', { timeZone: timeZone });
      timeElement.textContent = time;
    }
  }
}

// Update time every minute
updateTime();
setInterval(updateTime, 60000);
